# sdvxl - Software Defined VXLANs

**Note: This software is in very, very early stages of development. Bugs are expected.**

sdvxl is a network orchestrator written in Python that automatically creates VXLANs (referred to as networks) on nodes. It has an intuitive CLI which allows users to add nodes to the inventory, add networks as well as add nodes to networks with IPs which can be changed on the fly. It automatically provisions the nodes over SSH, meaning the nodes do not have to have an agent installed.

## Installation

To install sdvxl, clone this repository. That's it!

## Usage

### Initialise nodes

To initialise a node, you need its IP (which has to be static and accessible over the underlay network, this is not an IP through VXLAN) and a user you can access over SSH which has sudo privileges. The node name can be anything you want, it is just used to refer to the node in the program. 

`python3 sdvxl.py init node <nodename> <ip> <username>`

### Delete node

`python3 sdvxl.py delete node <nodename>`

### Create a network

To create a network (this is a VXLAN), you need to specify its VNI (if unsule, just start from 5001) and its subnet in CIDR form (like 10.10.1.0/24)

`python3 sdvxl.py create network <netname> <vni> <subnet>`

### Delete a network

`python3 sdvxl.py delete network <netname>`

### Add network to a node

To add a node to a network, you need to create a network that you want to assign it to and an IP inside the subnet you specified in create network **in CIDR form**. Please double check your IP is in CIDR form, like 10.10.1.23/24.

`python3 sdvxl.py node add network <netname> <nodename> <ip>`

### Delete network from node

`python3 sdvxl.py node delete network <netname> <nodename>`

### Show networks

To show a network and its details, you need the name of the network or use "all" to show all networks. Eventually this will be formatted, but right now this just outputs the releveant part of the inventory in dictionary form.

`python3 sdvxl.py show network <netname/all>`

### Show nodes

`python3 sdvxl.py show node <nodename/all>`

### Provision nodes

This command will provision all nodes with all of the config changes you are made. Provisioning consists of installing a startup script in `/sdvxl/startup.sh` and adding this script to the root crontab. -r will reboot the nodes after they have been provisioned.

`python3 sdvxl.py provision [-r]`

### Delete inventory file

To delete the inventory file (for example if the inventory file is messed up beyond repair), run the following: *Warning: This will delete the inventory file, without making a backup.*

`python3 sdvxl.py purge`

## Example

This example will create 2 nodes named node1 and node2 with external IPs of 10.0.0.21 and 10.0.0.22, and create a network called net1 with a subnet of 10.10.1.0/24 and VNI of 5101. node1 will be assigned the IP 10.10.1.1 in this network and node2 will be assigned the IP of 10.10.1.2 in net1. It will then provision the nodes.

```
python3 sdvxl.py init node node1 10.0.0.21 root
python3 sdvxl.py init node node2 10.0.0.22 root
python3 sdvxl.py create network net1 5101 10.10.1.0/24
python3 sdvxl.py node add network net1 node1 10.10.1.1/24 
python3 sdvxl.py node add network net1 node2 10.10.1.2/24
python3 sdvxl.py provision
```
