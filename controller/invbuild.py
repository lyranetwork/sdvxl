import socket

def getUnicastIps(name, inv):
    ips = []
    for nodeName in inv["networks"][name]["nodes"]:
        ips.append(inv["nodes"][nodeName]["ip"])

    return ips

def nodeGen(vni, ip, extip, unicastAddrs):
    bridgeStr = ""
    print(extip)

    for addr in unicastAddrs:
        addr = socket.gethostbyname(addr)
        bridgeStr += f"\nbridge fdb append to 00:00:00:00:00:00 dst {addr} dev vxlan{vni}" if addr != extip else ""
    bridgeStr = bridgeStr.strip()

    return f"""\nip link delete vxlan{vni}
ip link add vxlan{vni} type vxlan id {vni} dev \\$main_interface dstport 0 local {socket.gethostbyname(extip)}
{bridgeStr}
ip link set vxlan{vni} up
ip addr add {ip} dev vxlan{vni}"""

def genScripts(inventory):
    scripts = {}
    networks = {}
    for node, nodeInfo in inventory["nodes"].items():

        newScr = """sudo rm -r /sdvxl/ &> /dev/null
sudo mkdir /sdvxl &> /dev/null
sudo bash -c "cat <<'EOF' >> /sdvxl/startup.sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
main_interface=$(ip route get 8.8.8.8 | awk -- '{{printf $5}}')"""

        for netName, ip in nodeInfo["networks"].items():
            vni = inventory["networks"][netName]["vni"]
            if vni not in networks:
                networks[vni] = [ip]  
            else:
                networks[vni].append(ip)

            newScr += nodeGen(vni, ip, inventory["nodes"][node]["ip"], getUnicastIps(netName, inventory))
            
        newScr += "\nEOF\"\nsudo chmod +x /sdvxl/startup.sh &> /dev/null\nsudo /./sdvxl/startup.sh &> /dev/null\nsudo crontab -l | sudo grep -v '/sdvxl/startup.sh'  | sudo crontab -\n(sudo crontab -l 2>/dev/null; sudo echo \"@reboot /sdvxl/startup.sh\") | sudo crontab -"
        scripts[node] = newScr
        print(f"Generated script for {node}.")
    
    return scripts
