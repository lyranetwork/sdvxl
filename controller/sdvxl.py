import pickle, os, argparse
import invbuild

DEBUG = False
inventory = {"networks": {}, "nodes": {}}

# loadInv loads the inventory and creates it if it doesn't exist
def loadInv():
    global inventory
    try:
        with open("inventory.pkl", "rb") as f:
            inventory = pickle.load(f)
    except:
        with open("inventory.pkl", "wb+") as f:
            pickle.dump(inventory, f)

# saveInv saves the inventory to disk
def saveInv():
    global inventory
    with open("inventory.pkl", "wb+") as f:
        pickle.dump(inventory, f)

# prints statement if debug is set (used throughout to prvide debug messages)
def debug(a):
    if DEBUG == True:
        print("DEBUG: ", a)

# getArgs gets and parses the arguments provided
def getArgs(parser):
    subparsers = parser.add_subparsers(dest="op")

    subparsers.required = True

    parserOpInit = subparsers.add_parser("init", help="Initialise node")
    parserOpCreate = subparsers.add_parser("create", help="Create object")
    parserOpNode = subparsers.add_parser("node", help="Add objects to nodes")
    parserOpDelete = subparsers.add_parser("delete", help="Deletes node or network")
    parserOpPurge = subparsers.add_parser("purge", help="Deletes inventory file")
    parserOpShow = subparsers.add_parser("show", help="Shows object")
    parserOpProvision = subparsers.add_parser("provision", help="Provisions nodes")

    subparserInit = parserOpInit.add_subparsers(dest="subop", required=True)
    subparserCreate = parserOpCreate.add_subparsers(dest="subop", required=True)
    subparserNode = parserOpNode.add_subparsers(dest="subop", required=True)
    subparserDelete = parserOpDelete.add_subparsers(dest="subop", required=True)
    subparserShow = parserOpShow.add_subparsers(dest="subop", required=True)

    parserOpProvision.add_argument("-r", "--reboot", action="store_true")

    parserInitNode = subparserInit.add_parser("node", help="Initialise node")
    parserCreateNetwork = subparserCreate.add_parser("network", help="Create network")
    parserNodeAdd = subparserNode.add_parser("add", help="Add object to node")
    parserNodeDelete = subparserNode.add_parser("delete", help="Delete object from node")
    parserDeleteNetwork = subparserDelete.add_parser("network", help="Delete network")
    parserDeleteNode = subparserDelete.add_parser("node", help="Delete node")
    parserShowNetwork = subparserShow.add_parser("network", help="Shows specified network")
    parserShowNode = subparserShow.add_parser("node", help="Shows specified node")

    parserInitNode.add_argument("node", metavar="nodeName", help="Node name")
    parserInitNode.add_argument("ip", metavar="ip", help="Node ip address")
    parserInitNode.add_argument("user", metavar="user", help="SSH username")
    parserInitNode.add_argument("-p", "--purge", action="store_true", help="Specify this to purge a nodes network config. This is only applicable when you are replacing a node (i.e new node has same name as old node). Default: False")

    parserCreateNetwork.add_argument("name", metavar="netName", help="Network name")
    parserCreateNetwork.add_argument("vni", metavar="vni", help="VNI for new network. If unsure, start from 5001, then for the next network put 5002 etc.")
    parserCreateNetwork.add_argument("subnet", metavar="subnet", help="Subnet for network in CIDR form")

    parserDeleteNetwork.add_argument("name", metavar="netName", help="Network name", choices=inventory["networks"])

    parserDeleteNode.add_argument("name", metavar="nodeName", help="Node name", choices=inventory["nodes"])

    parserShowNetwork.add_argument("name", metavar="netName", help="Network name, enter 'all' to show all", choices=list(inventory["networks"].keys())+["all"])

    parserShowNode.add_argument("name", metavar="nodeName", help="Node name, enter 'all' to show all", choices=list(inventory["nodes"].keys())+["all"])

    subparserNodeAdd = parserNodeAdd.add_subparsers(dest="nodeOp", required=True)
    parserNodeAddNetwork = subparserNodeAdd.add_parser("network", help="Add network to node")

    subparserNodeDelete = parserNodeDelete.add_subparsers(dest="nodeOp", required=True)
    parserNodeDeleteNetwork = subparserNodeDelete.add_parser("network", help="Delete network from node")

    parserNodeAddNetwork.add_argument("network", metavar="netName", help="Network name", choices=inventory["networks"])
    parserNodeAddNetwork.add_argument("node", metavar="nodeName", help="Node name", choices=inventory["nodes"])
    parserNodeAddNetwork.add_argument("ip", metavar="ip", help="IP to assign node in this network in CIDR form (x.x.x.x/x)")

    parserNodeDeleteNetwork.add_argument("network", metavar="netName", help="Network name", choices=inventory["networks"])
    parserNodeDeleteNetwork.add_argument("node", metavar="nodeName", help="Node name", choices=inventory["nodes"])

    args = vars(parser.parse_args())
    return args

def initNode(node): # initialises node to inv
    global inventory
    debug(node)
    if node["node"] in inventory["nodes"] and node["purge"] == False:
        inventory["nodes"][node["node"]] = {"ip": node["ip"], "user": node["user"], "networks": inventory["nodes"][node["node"]]["networks"]}
    else:
        inventory["nodes"][node["node"]] = {"ip": node["ip"], "user": node["user"], "networks": {}}
    saveInv()

def showNode(node): # shows nodes
    if node["name"] != "all":
        print(inventory["nodes"][node["name"]])
    else:
        print(inventory["nodes"])

def addNet(network): # adds vxlan to inv
    global inventory
    inventory["networks"][network["name"]] = {"vni": network["vni"], "subnet": network["subnet"], "nodes": []}
    saveInv()

def delNet(network): # deletes vxlan from inv
    del inventory["networks"][network["name"]]
    for nodeName, node in inventory["nodes"].items():
        if network["name"] in node["networks"]:
            del inventory["nodes"][nodeName]["networks"][network["name"]]
    saveInv()

def showNet(network): # shows specified network/all networks
    if network["name"] != "all":
        print(inventory["networks"][network["name"]])
    else:
        print(inventory["networks"])

def purge(): # purges inv file
    global inventory
    print("Purging inventory file.")
    inventory = {"networks": {}, "nodes": {}}
    saveInv()

def addNetToNode(info): # adds node to network
    global inventory
    inventory["nodes"][info["node"]]["networks"][info["network"]] = info["ip"]
    inventory["networks"][info["network"]]["nodes"].append(info["node"]) if info["node"] not in inventory["networks"][info["network"]]["nodes"] else 0
    saveInv()

def delNetFromNode(info): # deletes node from network
    global inventory
    del inventory["nodes"][info["node"]]["networks"][info["network"]]
    inventory["networks"][info["network"]]["nodes"].remove(info["node"])
    saveInv()

def makeScripts(script): # gets generated scripts and writes them as files
    for node, scr in script.items():
        with open(f"{node}.sh", "w") as f:
            f.write(scr)

def delNode(node): # deletes node from inv
    del inventory["nodes"][node["name"]]
    for network, info in inventory["networks"].items():
        if node["name"] in info["nodes"]:
            inventory["networks"][network]["nodes"].remove(node["name"])
    saveInv()

def provision(reboot): # provisons nodes
    print("Generating scripts...")
    scripts = invbuild.genScripts(inventory)
    debug(scripts)
    makeScripts(scripts)

    for node, info in inventory["nodes"].items():
        print(f"Provisioning {node}.")
        os.system(f"ssh {info['user']}@{info['ip'].split('/')[0]} \"sudo -A bash -s\" < {node}.sh")
        print(f"Provisioned {node}.")

        if reboot == True:
            os.system(f"ssh {info['user']}@{info['ip'].split('/')[0]} \"sudo -A reboot\"")
            print(f"Rebooted {node}.")

# loads inventory, gets arguments and calls required fuctions.
def main():
    loadInv()
    parser = argparse.ArgumentParser(description = "sdvxl is a network orchestrator used to create networks and provision them on the fly using VXLANs to linux nodes. Check it out at https://gitlab.com/lyranetwork/sdvxl")
    args = getArgs(parser)

    if args["op"] == "create":
        if args["subop"] == "network":
            addNet(args)
        else:
            print("Invalid arguments.")
    elif args["op"] == "delete":
        if args["subop"] == "network":
            delNet(args)
        elif args["subop"] == "node":
            delNode(args)
        else:
            print("Invalid arguments.")
    elif args["op"] == "init":
        initNode(args)
    elif args["op"] == "node":
        if args["subop"] == "add":
            if args["nodeOp"] == "network":
                addNetToNode(args)
            else:
                print("Invalid arguments.")
        elif args["subop"] == "delete":
            if args["nodeOp"] == "network":
                delNetFromNode(args)
            else:
                print("Invalid arguments.")
        else:
            print("Invalid arguments.")
    elif args["op"] == "purge":
        purge()
    elif args["op"] == "show":
        if args["subop"] == "network":
            showNet(args)
        elif args["subop"] == "node":
            showNode(args)
        else:
            print("Invalid arguments.")
    elif args["op"] == "provision":
        provision(args["reboot"])
    else:
        print("Invalid arguments.")

if __name__ == "__main__":
    main()